package kmp;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
public class KMP {
	
	int numOfComparison = 0;
	public KMP() {
		// TODO Auto-generated constructor stub
	}
	public static void main(String[] args) throws FileNotFoundException{
		// TODO Auto-generated method stub
		KMP kmp = new KMP();
		String DNA="";
		boolean first = true;
        try (Scanner sc = new Scanner(new File("input.fasta"))) {
            while (sc.hasNextLine()) {
                String line = sc.nextLine().trim();
                if(line.length() <= 0) continue;
                if (line.charAt(0) != '>') {                  
                    DNA += line;
                }
            }
        }
		Scanner sc = new Scanner(System.in);		
		String p;
		System.out.println(DNA);
		System.out.println("Enter Pattern:");
		p = sc.nextLine();
		kmp.search(p,DNA);
		}
	public int[] buildPrefixTable(String pattern) {
		int M = pattern.length();
		int[] prefixTable= new int[M];
		prefixTable[0]=0;
		int i=1,j=0;;
		while(i<M) {
			if(pattern.charAt(j)== pattern.charAt(i)) {
				prefixTable[i]=j+1;
				i++;
				j++;
			}
			else {
				if(j !=0) {
					j = prefixTable[j-1];
				}
				else {
					prefixTable[i]=0;
					i++;
				}	
			}
		}
		return prefixTable;	
	}
	
	public void search(String pattern, String text) {
		int M = pattern.length();
		int N = text.length();
		
		int[] prefixTable = new int[M];
		prefixTable = buildPrefixTable(pattern);
		int i=0,j =0;
		while(i<N) {
			if(text.charAt(i)==pattern.charAt(j)) {
				i++;
				j++;
			}
			else {
				if(j != 0) {
					j = prefixTable[j-1];
				}
				else {
					i++;
				}				
			}
			if(j==M) {
				System.out.println(i-j);
				j = prefixTable[j-1];
			}
		}			
	}
		
}
